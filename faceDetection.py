import cv2
import imageio

faceCascade = cv2.CascadeClassifier('haarcascade-frontalface-default.xml')
eyeCascade = cv2.CascadeClassifier('haarcascade-eye.xml')

# Asagidaki fksda resim uzerinde yuz aramasi yapilacak ve yuz bulunduktan sonra o bolge isaretlenip dondurulecek.
#Yani aldigi resmi uzerine dikdortgen cizerek geri donderecek. Bu yuzden parametre olarak bir resim almasi gerekiyor.

def detect(frame):

    #blue green red to gray resmi siyah beyaza cevirdik.
    grayImage = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    #Resimde tespit edilen yuzlerin koordinatini almamiz gerekiyor.
    #Yüz tanımayı cascadeClassifier sınıfındaki detectMultiscale fks uyla gerceklestiriyoruz.
    #Faces a koordinatları atıyoruz.
    faces = faceCascade.detectMultiScale(grayImage, 1.3, 5)
    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 0, 0), 2)

    #Bu kisima kadar resmi siyah beyaza cevirdik.
    #Detect multi scale ile resimdeki yuzleri tespit ettik.
    #Her yüze bir dikdortgen cizebilmek icin bir for dongusu olusturduk.
    #Bu dongu resimde bulunan yuz sayisi kadar donecek.
    #Rectangle ile koordinatlarini onceden aldigimiz yuzlere dikdortgen cizdik.
    #Artik gozu yuz icinde arayacagiz.

        grayFace = grayImage[y:y+h, x:x+w]
        coloredFace = frame[y:y+h, x:x+w]
        eyes = eyeCascade.detectMultiScale(grayFace, 1.1, 3)
        #Artık gozu tespit edip koordinatlarini, yuksekligini ve genisligini almis olduk.
        for (ex, ey, ew, eh) in eyes:
            cv2.rectangle(coloredFace, (ex, ey), (ex+ew, ey+eh), (0, 255, 0), 2)

    return frame

#Bu fks.la videoyu aciyoruz.
reader = imageio.get_reader('bet.mov')
#fps video akarken saniyede gosterilen fotograf sayisi(genelde 25 tane olur.)
fps = reader.get_meta_data()['fps']
#Videoyu aldik simdi imageio ile output videosu olusturcaz.
#fps = fps -> aldigimiz video 30 fps ise output videosu da 30 fps olsun.
writer = imageio.get_writer('output.mov', fps=fps)
#Reader ile aldigimiz her framede for dongusu donucek.
for i, frame in enumerate(reader):
    frame =detect(frame)
    writer.append_data(frame)
    print(i) #bilgilendirme amacli

writer.close()

































